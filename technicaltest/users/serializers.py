from rest_framework import serializers

from .models import CustomUser


class CustomUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('email', 'first_name', 'last_name')

    def create(self, validated_data):
        return CustomUser(**validated_data)