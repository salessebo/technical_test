from abc import ABC, abstractmethod
from .models import CustomUser
from .serializers import CustomUserSerializer


class AbstractDataSource(ABC):
    @abstractmethod
    def query(self, database, **kwargs):
        raise NotImplementedError

    @property
    @abstractmethod
    def name(self):
        raise NotImplementedError


class DataSource(AbstractDataSource):
    def __init__(self, source='default'):
        self.source = source

    def query(self, **kwargs):
        result = CustomUser.objects.using(self.source).filter(**kwargs)
        custom_users_serializer = CustomUserSerializer(result, many=True)
        return custom_users_serializer.data

    def name(self):
        return self.source
