from django.http.response import JsonResponse
from django.views import View
from rest_framework.parsers import JSONParser

from .serializers import CustomUserSerializer
from .datasource import DataSource

import concurrent.futures


class DataSourceView(View):

    @staticmethod
    def get(request):
        query_dict = request.GET.dict()
        ds1, ds2 = DataSource(), DataSource("users")

        with concurrent.futures.ThreadPoolExecutor() as thread_pool:
            future1 = thread_pool.submit(ds1.query, **query_dict)
            future2 = thread_pool.submit(ds2.query, **query_dict)
            response = {ds1.name(): future1.result(), ds2.name(): future2.result()}

        return JsonResponse(response, safe=False)

    @staticmethod
    def post(request):
        custom_users_data = JSONParser().parse(request)
        custom_users_serializer = CustomUserSerializer(data=custom_users_data)

        if custom_users_serializer.is_valid():
            custom_user = custom_users_serializer.create(custom_users_data)
            custom_user.save(using="default")
            custom_user.save(using="users")
            return JsonResponse("Successfully Added", safe=False)
        return JsonResponse("Failed to add user", safe=False)



